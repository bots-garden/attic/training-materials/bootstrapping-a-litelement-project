import { LitElement, html } from 'lit-element';

import {style} from './main-styles.js';

export class MySubTitle extends LitElement {

  static get properties() {
    return {
      myText: { attribute: 'my-text' }
    }
  }

  constructor() {
    super()
    if(window.components == null) window.components = []
    window.components.push(this)
  }

  render(){
    return html`
      ${style}
      <h2 class="subtitle">
        ${this.myText}
      </h2>    
    `
  }

}
customElements.define('my-sub-title', MySubTitle)